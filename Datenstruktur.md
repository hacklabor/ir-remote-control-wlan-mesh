## ESP File1 (Meta.txt) combobox1

- Ein Gerät je Zeile Trenner "CRLF" 

Zeilenaufbau

- Zeichen Nr. 1-2 (INTEGER) Geräte ID
- Zeichen Nr. 3-3  (BYTE) RC Code
- Zeichen Nr. 4-20 (char[16]) NAME des Gerätes 

## ESP File2 (rccode.txt) combobox2 in abhängikeit von Combobox1 ID

- Ein Steuerungscode je Zeile Trenner "CRLF" 

Zeilenaufbau

- Zeichen Nr. 1-2 (Integer) Geräte ID
- Zeichen Nr. 2-10 (char[10]) Tastenname
- restliche Zeichen (HEX[?]) IR Steuerungscodes
  


#  !!!ODER!!

- Metafile Gerätename ReciverName.txt Zeilenummer = ID
- Pro Zeile ein Gerätename (char[16])

------


- je Gerät eine Datei (RCreciver+ID.txt) dann mus man nicht lange suchen
- Zeile 1 ID (INT16)
- Zeile 2 RC Protokoll ID (BYTE)
- Zeile 3 RC Tastenname (char[10])+Wiederholung[byte]+ Rest der Zeile RC Hex[] Steuerungscode
- .
- .
- .
- .
- Zeile 9999999 RC Tastenname (char[10])+Wiederholung[byte]+ Rest der Zeile RC Hex[] Steuerungscode