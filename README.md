# IR Remote Control WLAN Mesh

WLAN Mesh network of infrared led transmitter

## Funktionalität

- Web Interface zum TEACHEN und Auslösen der Steurbefehle
- an jeder IR Einheit = CUBE (WEMOS D1 mini + IR Controler Shield) könen neue Befehle eingelernt werden. Diese verteilen sich automatisch an weitere IR CUBES im selben Netzwerk. Somit backupen sich die CUBES gegenseitig.
- Datenprotokoll Websocket
- ein ausgelöster IR Sende Befehl wird automatisch über MQTT auch an allen anderen CUBES ausgelöst
- Einbindung in NODE RED über Mqtt , NodeRed ist aber für die Funktion nicht zwingend erforderlich sondern stellt nur noch eine andere Bedienmöglichkeit da. Wie z.B alles AUS

## links
[RC5 Code****  https://www.sprut.de/electronic/ir/rc5.htm#2](https://www.sprut.de/electronic/ir/rc5.htm#2)