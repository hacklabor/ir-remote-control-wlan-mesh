


#include <FS.h>

#include "secrets.h"
//OTA
#include <ArduinoOTA.h>
// ESP 8266
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
// size of buffer used to capture HTTP requests
#define REQ_BUF_SZ   60
extern "C" {
#include "user_interface.h" //to set the hostname
}

#ifndef DEBUG
#define DEBUG false  // Change to 'false' to disable all serial output.
#endif  // DEBUG

WiFiServer server1(80);
WiFiClient espClient;
PubSubClient client(espClient);

char HTTP_req[REQ_BUF_SZ] = { 0 }; // buffered HTTP request stored as null terminated string
char req_index = 0;              // index into HTTP_req buffer
byte ID_Rc = 255;
byte ID_Bt = 255;


void debug(const char* str) {
#if DEBUG
	uint32_t now = millis();
	Serial.printf("%07u.%03u: %s\n", now / 1000, now % 1000, str);
#endif  // DEBUG
}

void setup() {
	//Serial start
	Serial.begin(115200);
	Serial.println("START");
	
	delay(50);

	// WiFi setup
	setup_wifi();

	// MQTT connect
	Serial.print("setup MQTT...");
	// connecting to the mqtt server
	// client.setServer(mqtt_server, 1883);
	// client.setCallback(callback);
	// Serial.println("MQTT done!");
	// client.publish("IRTX", "50");
	// client.publish("IR_UPDATE", "50");

	// get Start Animation
	
	
	server1.begin();
}

void setup_wifi() {
	delay(10);
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(ssid);

	wifi_station_set_hostname(espHostname); //set ESP hostname
	WiFi.mode(WIFI_STA); // set Wifi mode
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}

	//OTA
	ArduinoOTA.setHostname(espHostname);
	ArduinoOTA.onStart([]() {
		String type;
		if (ArduinoOTA.getCommand() == U_FLASH) {
			type = "sketch";
		}
		else { // U_SPIFFS
			type = "filesystem";
		}

		// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
		Serial.println("Start updating " + type);
	});
	ArduinoOTA.onEnd([]() {
		Serial.println("\nEnd");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
	});
	ArduinoOTA.onError([](ota_error_t error) {
		Serial.printf("Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR) {
			Serial.println("Auth Failed");
		}
		else if (error == OTA_BEGIN_ERROR) {
			Serial.println("Begin Failed");
		}
		else if (error == OTA_CONNECT_ERROR) {
			Serial.println("Connect Failed");
		}
		else if (error == OTA_RECEIVE_ERROR) {
			Serial.println("Receive Failed");
		}
		else if (error == OTA_END_ERROR) {
			Serial.println("End Failed");
		}
	});
	ArduinoOTA.begin();

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());

}

void callback(char* topic, byte* payload, unsigned int length) {

	Serial.print("Message arrived [");
	Serial.print(topic);
	Serial.print("] ");
	String color("#");
	String command;
	for (int i = 0; i < length; i++) {
		Serial.print((char)payload[i]);
		command = command + String((char)payload[i]);
	}

	Serial.println();


	// finding Color(hex)payload
	if ((char)payload[0] == '#') {
		// setting color
		command.remove(0, 1);
	}
	else if ((char)payload[0] == '%') {
		command.remove(0, 1);

	}
}
void reconnect() {
	// Loop until we're reconnected
	while (!client.connected()) {
		Serial.print("Attempting MQTT connection...");
		// Attempt to connect
		if (client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage)) {
			Serial.println("connected");

			// Once connected, publish an announcement...
			client.publish(willTopic, willOnMessage, willRetain);
			// ... and resubscribe
			client.subscribe(mainTopic);
		}
		else {
			Serial.print("failed, rc=");
			Serial.print(client.state());
			Serial.println(" try again in 5 seconds");
			// Wait 5 seconds before retrying
			delay(5000);
		}
	}
}

//*****************Begin WEB Server Funktionen************************
// searches for the string sfind in the string str
// returns 1 if string found
// returns 0 if string not found
char StrContains(char* str, char* sfind)
{
	char found = 0;
	char index = 0;
	char len;

	len = strlen(str);

	if (strlen(sfind) > len) {
		return 0;
	}
	while (index < len) {
		if (str[index] == sfind[found]) {
			found++;
			if (strlen(sfind) == found) {
				return 1;
			}
		}
		else {
			found = 0;
		}
		index++;
	}

	return 0;
}



// sets every element of str to 0 (clears array)
void StrClear(char* str, char length)
{
	for (int i = 0; i < length; i++) {
		str[i] = 0;
	}
}
void serverHtml() {
#define BUFS 100
	char clientline[BUFS];
	int index = 0;
	String TMPSTRING = "";

	WiFiClient client1 = server1.available();
	if (client1) {  // got client?
		boolean currentLineIsBlank = true;
		while (client1.connected()) {
			if (client1.available()) {   // client data available to read
				char c = client1.read(); // read 1 byte (character) from client
				//Serial.print(c);
				// limit the size of the stored received HTTP request
				// buffer first part of HTTP request in HTTP_req array (string)
				// leave last element in array as 0 to null terminate string (REQ_BUF_SZ - 1)
				if (req_index < (REQ_BUF_SZ - 1)) {
					HTTP_req[req_index] = c;          // save HTTP request character
					req_index++;
				}
				// last line of client request is blank and ends with \n
				// respond to client only after last line received
				if (c == '\n' && currentLineIsBlank) {
					// send a standard http response header
					client1.println("HTTP/1.1 200 OK");
					Serial.print("Response: ");
					// remainder of header follows below, depending on if
					// web page or XML page is requested
					// Ajax request - send XML file
					if (StrContains(HTTP_req, "RC_ID")) {
						// send rest of HTTP header
						client1.println("Content-Type: text/xml");
						client1.println("Connection: keep-alive");
						client1.println();
						// print the received hour and minute to the Serial Monitor window
						// if received with the incoming HTTP GET string
						char* str_part;              // pointer to part of the HTTP_req string
						char str[3] = { 0 };      // used to extract the times from HTTP_req string

						// get pointer to the beginning of hour data in string
						str_part = strstr(HTTP_req, "&h=");
						
						if (str_part != NULL) {
							// get the hour from the string

							 

							Serial.print("RC_ID: ");
						
							String ID = &str_part[3];
							ID += &str_part[4];
							ID_Rc = ID.toInt();
							Serial.println(ID_Rc);
							
							
							
							
							
						}
					}
					if (StrContains(HTTP_req, "RC_BT")) {
						// send rest of HTTP header
						client1.println("Content-Type: text/xml");
						client1.println("Connection: keep-alive");
						client1.println();
						// print the received hour and minute to the Serial Monitor window
						// if received with the incoming HTTP GET string
						char* str_part;              // pointer to part of the HTTP_req string
						char str_time[3] = { 0 };      // used to extract the times from HTTP_req string

						// get pointer to the beginning of hour data in string
						str_part = strstr(HTTP_req, "&h=");
						
						if (str_part != NULL) {
							// get the hour from the string
							Serial.print("RC_BT: ");
							String ID = &str_part[3];
							ID += &str_part[4];
							ID_Bt = ID.toInt();
							Serial.println(ID_Bt);
							


						}
					}
				else {  // web page request
					// send rest of HTTP header
					client1.println("Content-Type: text/html");
					client1.println("Connection: keep-alive");
					client1.println();

				}
					
					client1.println("<!DOCTYPE html>");
					client1.println("	<html lang = \"en\">");
					client1.println("<head>");
					client1.println("	<meta name = \"viewport\" content = \"width=device-width, initial-scale=1\">");
					client1.println("	<link rel = \"icon\" href = \"data:,\">");
				
					client1.println("	<script>");
					client1.println(file_Read("/script.js"));
					client1.println("	</script>");

					client1.println("	<style>");
					client1.println(file_Read("/styles.css"));
					client1.println("	</style>");



					
					client1.println("</head>");
					client1.println("	<body>");
					client1.println("		<select id=\"RCD\" onchange=\"GetRCID()\">");
				
					client1.println("			<option value = '00'>Test00 </option>");
					client1.println("			<option value = '01'>Test01 </option>");
					client1.println("			<option value = '02'>Test02 </option>");
					client1.println("			<option value = '03'>Test03 </option>");
					client1.println("	    </select>");

					client1.println("<input id=\"RC_text\" type = \"text\" maxlength=\"10\" minlength=\"1\" value = \"Mouse\"><br>");

					client1.println("<div class=\"BtMain\"> ");
					client1.println("    ");
					client1.println("    <button class=\"MAIN_Bt\" onclick=\"sendID('99')\"> NEW RC </button>");
					client1.println("    <button class=\"MAIN_Bt\" onclick=\"sendID('98')\"> �ndern </button>");
					client1.println("    <button class=\"MAIN_Bt\" onclick=\"sendID('97')\"> TEACH </button>");
					client1.println("</div>");

					client1.println("<div class=\"RemoteControl\"> ");
					client1.println("    ");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('00')\">ON</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('01')\">OFF</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('02')\">INPUT</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('03')\">VOL+</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('04')\">Menu</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('05')\">Ch+</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('06')\">Vol-</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('07')\">RET</button>");
					client1.println("    <button class=\"RC_Bt\" onclick=\"sendID('08')\">CH-</button>");
					client1.println("</div>");
					

					client1.println("	</body></html>");
						client1.println();
						// und wir verlassen mit einem break die Schleife
						 // reset buffer index and all buffer elements to 0
						req_index = 0;
						StrClear(HTTP_req, REQ_BUF_SZ);
						break;
				}
				// every line of text received from the client ends with \r\n
				if (c == '\n') {
					// last character on line of received text
					// starting new line with next character read
					currentLineIsBlank = true;
				}
				else if (c != '\r') {
					// a text character was received from client
					currentLineIsBlank = false;
				}
			} // end if (client.available())
		} // end while (client.connected())
		delay(1);      // give the web browser time to receive the data
		client1.stop(); // close the connection
		Serial.println("Client getrennt.");
		Serial.println("");
	}
	
}



//*****************Ende WEB Server Funktionen************************
String file_Read(String FileName) {
	Serial.print(FileName);
	
	if (SPIFFS.begin()) {
		debug("mounted file system");
		if (SPIFFS.exists(FileName)) {
			debug("Data file exists");
			File dataFile = SPIFFS.open(FileName, "r");
			if (dataFile) {

				size_t size = dataFile.size();
				Serial.print(" Dateisize:");
				Serial.print(size);
				Serial.println(" Bytes");
				// Allocate a buffer to store contents of the file.
				std::unique_ptr<char[]> buf(new char[size]);
				debug("READ DATA FILE");
				String txt = dataFile.readString();
								dataFile.close();
				SPIFFS.end();
				debug("Fertig READ DATA FILE");
				return txt;
			}
			dataFile.close();
		}
		SPIFFS.end();
	}
	return "FEHLER";
	
}
	



void loop() {
	
	if (!client.connected()) {
		//delay(100);
		//reconnect();
	}
	//client.loop();
	ArduinoOTA.handle();
	serverHtml();
	

	


	delay(10);


}



